package open.alien.nostromo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;

@EntityScan
@EnableJpaRepositories
@EnableDiscoveryClient
@SpringBootApplication
@EnableCaching
@EnableFeignClients
@ComponentScan
@EnableHystrix
public class NostromoApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(NostromoApplication.class, args);
	}

}
