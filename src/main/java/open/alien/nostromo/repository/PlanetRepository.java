package open.alien.nostromo.repository;


import open.alien.nostromo.model.Planet;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlanetRepository extends CrudRepository<Planet, Integer> {

    @Cacheable(value="findByName", unless="#result == null")
    Planet findByName(String name);

    List<Planet> findAllByDiscovererIn(String[] discoverers);
}
