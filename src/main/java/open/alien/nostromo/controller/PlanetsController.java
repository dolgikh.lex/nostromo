package open.alien.nostromo.controller;

import io.swagger.annotations.ApiOperation;
import open.alien.nostromo.model.Planet;
import open.alien.nostromo.repository.PlanetRepository;
import open.alien.nostromo.service.ShuttlePlanetSearchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/planets")
public class PlanetsController {

    private PlanetRepository planetRepository;
    private ShuttlePlanetSearchService shuttlePlanetSearchService;

    public PlanetsController(ShuttlePlanetSearchService shuttlePlanetSearchService,
                             PlanetRepository planetRepository) {
        this.shuttlePlanetSearchService = shuttlePlanetSearchService;
        this.planetRepository = planetRepository;
    }

    @ApiOperation(value = "Ask shuttle to discover planets", response = String.class)
    @GetMapping(value = "/discover", params = {"name"})
    public ResponseEntity discoverPlanets(@RequestParam(value = "name") String[] planetsName) {
        return new ResponseEntity<>(shuttlePlanetSearchService.discover(planetsName), HttpStatus.OK);
    }

    @ApiOperation(value = "Get discovered planet by name", response = String.class)
    @GetMapping("/{name}")
    public ResponseEntity getPlanetByName(@PathVariable String name) {
        Planet planet = planetRepository.findByName(name);
        if (planet == null) {
            return new ResponseEntity<>("Sorry, but " + name + " have not discovered yet", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(planet.getText(), HttpStatus.OK);
    }

    @ApiOperation(value = "Get discovered planets by Shuttles tag", response = String.class)
    @GetMapping(value = "/", params = {"processedBy"})
    public ResponseEntity getPlanetsByShuttles(@RequestParam(value = "processedBy") String[] shuttlesName) {
        return new ResponseEntity<>(planetRepository.findAllByDiscovererIn(shuttlesName), HttpStatus.OK);
    }
}
