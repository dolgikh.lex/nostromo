package open.alien.nostromo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shuttle {

    public String getAddress() {
        return address;
    }

    @JsonProperty("Address")
    private String address;

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("ServiceTags")
    private List<String> serviceTags = new ArrayList<>();

    @JsonProperty("ServiceName")
    private String serviceName;

    public String getServicePort() {
        return servicePort;
    }

    @JsonProperty("ServicePort")
    private String servicePort;

    @JsonProperty("ServiceID")
    private String serviceId;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public List<String> getServiceTags() {
        return serviceTags;
    }

}