package open.alien.nostromo.model;

import org.springframework.stereotype.Component;

@Component
public class HystrixClientFallback implements ShuttleFeignClient{
    @Override
    public ShuttleResponse discover(String planet) {
        System.out.println("Hello Fallback!!!");
        return new ShuttleResponse();
    }
}
