package open.alien.nostromo.model;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "SHUTTLE", fallback = HystrixClientFallback.class)
public interface ShuttleFeignClient {
    @RequestMapping(value = "/discovery/?target={planet}")
    ShuttleResponse discover(@PathVariable("planet") String planet);
}