package open.alien.nostromo.model;


public class ShuttleResponse {
    private String error;

    private String response;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ShuttleResponse{" +
                "error='" + error + '\'' +
                ", response='" + response + '\'' +
                '}';
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
