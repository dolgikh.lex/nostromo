package open.alien.nostromo.service;

import io.reactivex.Observable;
import open.alien.nostromo.model.Shuttle;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Service
public class ObservableShuttleConfiguration {
    private BlockingQueue<Shuttle> shuttlesQueue;
    private List<Shuttle> shuttlesList = null;

    private Observable<Shuttle> observableShuttle() {
        return Observable.create(emitter -> {
            for (Shuttle shuttle : shuttlesQueue) {
                emitter.onNext(shuttle);
            }
            emitter.onComplete();

        });
    }
    public void setShuttlesList(List<Shuttle> shuttles) {
        if (shuttlesList == null || shuttlesList.size() != shuttles.size()){
            shuttlesList = shuttles;
            BlockingQueue<Shuttle> queue = new ArrayBlockingQueue<>(shuttles.size());
            queue.addAll(shuttles);
            this.shuttlesQueue = queue;
        }
    }

    public Observable<Shuttle> getObservableShuttles() {
        return observableShuttle();
    }

    public BlockingQueue<Shuttle> getShuttles() {
        return shuttlesQueue;
    }

    public List<Shuttle> getShuttlesList() {
        return shuttlesList;
    }
}
