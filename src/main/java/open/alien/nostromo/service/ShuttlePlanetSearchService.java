package open.alien.nostromo.service;

import open.alien.nostromo.controller.PlanetsController;
import open.alien.nostromo.model.Planet;
import open.alien.nostromo.model.ShuttleFeignClient;
import open.alien.nostromo.model.ShuttleResponse;
import open.alien.nostromo.repository.PlanetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.PriorityQueue;
import java.util.Queue;


@Component
public class ShuttlePlanetSearchService {

    private static Logger logger = LoggerFactory.getLogger(PlanetsController.class);

    private ShuttleFeignClient shuttleFeignClient;
    private PlanetRepository planetRepository;
    private Queue<String> planets = new PriorityQueue<>();

    @Value("${planets.list.maxquantity}")
    private int planetsListSize;

    public ShuttlePlanetSearchService(ShuttleFeignClient shuttleFeignClient, PlanetRepository planetRepository) {
        this.planetRepository = planetRepository;
        this.shuttleFeignClient = shuttleFeignClient;
    }

   // @HystrixCommand(fallbackMethod = "discover")
    public String discover(String[] planetNames) {
        String error = getValidateErrors(planetNames);
        if (error != null)
            return error;

        subscribeOnShuttles();

        return "Done";
    }

    private String getValidateErrors(String[] planetNames) {
        for (String planetName : planetNames) {
            if (planetRepository.findByName(planetName) == null)
                planets.add(planetName);
        }
        return null;
    }

    private void subscribeOnShuttles() {
        while (!planets.isEmpty()) {
            String planetName = planets.poll();
            System.out.println("Request for: " + planetName);

            ShuttleResponse responseJSON;
            try {
                responseJSON = shuttleFeignClient.discover(planetName);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.printf("ERROR request shuttle " + planetName);
                planets.add(planetName);
                return;
            }

            System.out.println("Got response. Go parse ...");
            if (responseJSON.getResponse() != null) {
                System.out.println("Task done!");
                Planet planet = new Planet();
                planet.setText(responseJSON.getResponse());
                planet.setName(planetName);
                planetRepository.save(planet);
            }
        }
    }
}
