FROM java:8
ADD https://services.gradle.org/distributions/gradle-4.1-bin.zip /
RUN unzip gradle-4.1-bin.zip -d /opt
ENV GRADLE_HOME /opt/gradle-4.1
ENV PATH $PATH:/opt/gradle-4.1/bin
ENV name default_nostromo_name
ENV port 8078
ENV tag default_tag
WORKDIR /project
ADD . /project
CMD java -Dspring.application.name=${name} -Dserver.port=${port} -Dspring.cloud.consul.discovery.tags=${tag} -jar target/nostromo-latest.jar
#CMD gradle bootRun -Dspring.application.name=${name} -Dserver.port=${port} -Dspring.cloud.consul.discovery.tags=${tag}