Логика:
Шаттлы запускаются с одинаковым названием - shuttle, а в тегах указывается само имя шаттла.

Команда для запуска шаттла:
gradle bootrun -Dspring.application.name=shuttle -Dserver.port=8080 -Dspring.cloud.consul.discovery.tags=first
